#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /app/data/attachments /run/sessions /tmp/webapp /tmp/spooler

if [[ ! -d /app/data/kopano_config ]]; then
    echo "=> First run, copy config templates"
    mkdir -p /app/data/kopano_config
    cp -rf /etc/kopano.orig/* /app/data/kopano_config
    cp -f /usr/share/doc/kopano/example-config/ldap.cfg /app/data/kopano_config/


    # overwrite values from /usr/share/kopano/ldap.openldap.cfg
    echo "ldap_user_type_attribute_value = user" >> /app/data/kopano_config/ldap.cfg
    echo "ldap_user_unique_attribute = uid" >> /app/data/kopano_config/ldap.cfg
    echo "ldap_loginname_attribute = username" >> /app/data/kopano_config/ldap.cfg
    echo "ldap_group_unique_attribute = cn" >> /app/data/kopano_config/ldap.cfg
    echo "ldap_fullname_attribute = displayname" >> /app/data/kopano_config/ldap.cfg

    # isAdmin is pending on https://git.cloudron.io/cloudron/box/issues/241
    echo "ldap_isadmin_attribute = isadmin" >> /app/data/kopano_config/ldap.cfg
fi

echo "=> Configure server"
sed -e "s/#log_level\s.*/log_level = 2/" -i /app/data/kopano_config/server.cfg   # enable disabled variables
sed -e "s/mysql_host\s.*/mysql_host = ${MYSQL_HOST}/" \
    -e "s/mysql_port\s.*/mysql_port = ${MYSQL_PORT}/" \
    -e "s/mysql_database\s.*/mysql_database = ${MYSQL_DATABASE}/" \
    -e "s/mysql_user\s.*/mysql_user = ${MYSQL_USERNAME}/" \
    -e "s/mysql_password\s.*/mysql_password = ${MYSQL_PASSWORD}/" \
    -e "s/attachment_path\s.*/attachment_path = \/app\/data\/attachments/" \
    -e "s/log_file\s.*/log_file = -/" \
    -e "s/log_level\s.*/log_level = 2/" \
    -e "s/user_plugin\s.*/user_plugin = ldap/" \
    -i /app/data/kopano_config/server.cfg

echo "=> Configure ldap"
sed -e "s/#ldap_port\s.*/ldap_port = ${LDAP_PORT}/" -i /app/data/kopano_config/ldap.cfg   # enable disabled variables
sed -e "s/ldap_host\s.*/ldap_host = ${LDAP_SERVER}/" \
    -e "s/ldap_port\s.*/ldap_port = ${LDAP_PORT}/" \
    -e "s/ldap_bind_user\s.*/ldap_bind_user = ${LDAP_BIND_DN}/" \
    -e "s/ldap_bind_passwd\s.*/ldap_bind_passwd = ${LDAP_BIND_PASSWORD}/" \
    -e "s/ldap_search_base\s.*/ldap_search_base = ${LDAP_USERS_BASE_DN}/" \
    -i /app/data/kopano_config/ldap.cfg

echo "=> Configure spooler"
sed -e "s/#log_level\s.*/log_level = 6/" -i /app/data/kopano_config/spooler.cfg   # enable disabled variables
sed -e "s/log_file\s.*/log_file = -/" \
    -e "s/log_level\s.*/log_level = 6/" \
    -e "s/tmp_path\s.*/tmp_path = \/tmp\/spooler\//" \
    -i /app/data/kopano_config/spooler.cfg

echo "=> Configure mail relay"
if [[ ! -d /app/data/postfix ]]; then
    mkdir -p /app/data/postfix
    cp -rf /etc/postfix.orig/* /app/data/postfix
    mkdir -p /app/data/postfix-spool
    chown -R postfix:postfix /app/data/postfix-spool
    /usr/sbin/postconf -e relayhost="$MAIL_SMTP_SERVER:$MAIL_SMTP_PORT";
    echo "$MAIL_SMTP_SERVER $MAIL_SMTP_USER:$MAIL_SMTP_PASSWORD" >> /etc/postfix/sasl_passwd
    /usr/sbin/postmap /etc/postfix/sasl_passwd
fi

echo "=> Ensure config ownership"
chown -R kopano:kopano /app/data /run /tmp
chown -R www-data:www-data /run/sessions /tmp/webapp

echo "=> Starting supervisord"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Kopano
