FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV SERIAL_KEY=ZSH6003SH66C9HUJRNJ72QSGX
ENV \
    CORE_REPO_URL=https://serial:${SERIAL_KEY}@download.kopano.io/supported/core:/final/Ubuntu_18.04/ \
    WEBAPP_REPO_URL=https://serial:${SERIAL_KEY}@download.kopano.io/supported/webapp:/final/Ubuntu_18.04/

# hadolint ignore=DL3008
RUN \
    echo "deb $CORE_REPO_URL /">> /etc/apt/sources.list.d/kopano.list && \
    echo "deb $WEBAPP_REPO_URL /">> /etc/apt/sources.list.d/kopano.list && \
    curl $CORE_REPO_URL/Release.key | apt-key add - && \
    apt-get update -y && \
    apt-get install -y --no-install-recommends \
    crudini \
    kopano-server-packages \
    kopano-webapp \
    kopano-webapp-plugin-desktopnotifications \
    kopano-webapp-plugin-filepreviewer \
    kopano-webapp-plugin-titlecounter && \
    rm -rf /var/cache/apt /var/lib/apt/lists/*

WORKDIR /app/code/

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log
COPY apache2-kopano.conf /etc/apache2/sites-available/kopano.conf
RUN ln -sf /etc/apache2/sites-available/kopano.conf /etc/apache2/sites-enabled/kopano.conf
RUN echo "Listen 80" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 500M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 500M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_input_vars 1800 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/sessions

# make the configs writable
RUN mv /etc/kopano /etc/kopano.orig && ln -s /app/data/kopano_config /etc/kopano

# allow the webapp to use a tmp folder
RUN rm -rf /var/lib/kopano-webapp/tmp && ln -sf /tmp/webapp /var/lib/kopano-webapp/tmp

# configure supervisor
COPY supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

# postfix mail relay
RUN echo "postfix postfix/mailname string $MAILNAME" | debconf-set-selections && \
    echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections && \
    apt-get install -y postfix && rm -rf /var/cache/apt /var/lib/apt/lists
RUN touch /etc/postfix/sasl_passwd /etc/postfix/virtual_regexp /etc/postfix/virtual /etc/postfix/sender_canonical_regexp && \
    /usr/sbin/postconf -e smtp_tls_security_level=may && \
    /usr/sbin/postconf -e smtp_sasl_auth_enable=yes && \
    /usr/sbin/postconf -e smtp_sasl_password_maps=hash:/etc/postfix/sasl_passwd && \
    /usr/sbin/postconf -e smtp_sasl_security_options=noanonymous && \
    /usr/sbin/postconf -e myhostname=localhost && \
    /usr/sbin/postconf -e mydomain=localdomain && \
    /usr/sbin/postconf -e mydestination=localhost && \
    /usr/sbin/postconf -e mynetworks='127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128' && \
    /usr/sbin/postconf -e inet_interfaces=loopback-only && \
    /usr/sbin/postconf -e smtp_helo_name=\$myhostname.\$mydomain && \
    /usr/sbin/postconf -e virtual_maps='hash:/etc/postfix/virtual, regexp:/etc/postfix/virtual_regexp' && \
    /usr/sbin/postconf -e sender_canonical_maps=regexp:/etc/postfix/sender_canonical_regexp && \
    /usr/sbin/postconf -e queue_directory=/app/data/postfix-spool && \
    /usr/sbin/postconf compatibility_level=2 && \
    /usr/sbin/postmap /etc/postfix/sasl_passwd && \
    /usr/sbin/postmap /etc/postfix/virtual_regexp && \
    /usr/sbin/postmap /etc/postfix/virtual && \
    /usr/sbin/postmap /etc/postfix/sender_canonical_regexp
RUN mv /etc/postfix /etc/postfix.orig && ln -s /app/data/postfix /etc/postfix

COPY start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
